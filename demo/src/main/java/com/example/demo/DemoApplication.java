package com.example.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

import org.jsoup.nodes.Document;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		Document document;
		try {
			document = Jsoup.connect("https://www.4icu.org/kh/a-z/").get();

			//get data table
			Element table = document.select("table").get(0);
			Elements rows = table.select("td:nth-of-type(2)");
			for(Element row : rows) {
				System.out.println(row.text());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
